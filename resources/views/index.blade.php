<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
    <script src="https://unpkg.com/http-vue-loader"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <title>Jumia Market</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
    <style>
        .clearfix {
            margin-bottom: 30px;
        }
    </style>
    <!-- Styles -->
</head>
<body>
<div class="container" id="app">

    <div class="row justify-content-md-center clearfix">
        <div class="col col-lg-2">
            <h3>Jumia Market</h3>
        </div>
    </div>
    <div class="row row-cols-4 clearfix">
        <div class="col">
            <select class="form-select form-select-sm" @change="filterValues" v-model="country"
                    aria-label=".form-select-sm example">
                <option v-bind:value="0">Please Select Country</option>

                <option v-for="country in countriesNames" :key="country.id" v-bind:value="country.id">
                    @{{country.name}}
                </option>
            </select>

        </div>
        <div class="col">
            <select class="form-select form-select-sm" @change="filterValues" v-model="filter"
                    aria-label=".form-select-sm example">
                <option v-bind:value="0">Valid phone numbers</option>
                <option value="1">Valid</option>
                <option value="2">Invalid</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Country</th>
                    <th scope="col">State</th>
                    <th scope="col">Country Code</th>
                    <th scope="col">Phone num</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="country in countriesList" :key="country.id">
                    <td>@{{country.name}}</td>
                    <td>@{{country.valid}}</td>
                    <td>@{{country.code}}</td>
                    <td>@{{country.phone}}</td>

                </tr>
                </tbody>
            </table>
        </div>
        <div class="col"></div>


    </div>
</div>
</body>
<footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"
            integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/"
            crossorigin="anonymous"></script>

    <script>

        new Vue({
            el: '#app',
            data() {
                return {
                    countriesList: [],
                    countriesNames: [],
                    country: 0,
                    filter: 0
                }
            },
            mounted() {
                axios
                    .get('http://localhost:8000/api/')
                    .then(response => (this.countriesList = response.data,
                        this.countriesNames = response.data))
            },
            methods: {
                filterValues() {
                    let country = this.country
                    let filter = this.filter
                    axios.get(`http://localhost:8000/api/?country=${country}&filter=${filter}`, {
                        'country': country,
                        'filter': filter
                    })
                        .then(response => (this.countriesList = response.data))
                },
            },

        })
    </script>
</footer>
</html>
