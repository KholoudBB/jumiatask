<?php

use App\Country;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers =
            [
                [
                    'phone' => '2121234567901',
                    'country_id' => Country::pluck('id')->random(),
                ],
                [
                    'phone' => '251931014712',
                    'country_id' => Country::pluck('id')->random(),
                ],
                [
                    'phone' => '212431013612',
                    'country_id' => Country::pluck('id')->random(),
                ],
                [
                    'phone' => '234431313512',
                    'country_id' => Country::pluck('id')->random(),
                ],
                [
                    'phone' => '234651513517',
                    'country_id' => Country::pluck('id')->random(),
                ]

            ];


        DB::table('customers')->insert($customers);

    }
}
