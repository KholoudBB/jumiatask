<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries =
            [
                [
                    'name' => 'Cameroon',
                    'code' => '+237',
                    'regex' => '~^251[1-9]\d{8,}$~D',
                ],
                [
                    'name' => 'Ethiopia',
                    'code' => '+251',
                    'regex' => '~^251[1-9]\d{8,}$~D',
                ],
                [
                    'name' => 'Morocco',
                    'code' => '+212',
                    'regex' => '~^212[1-9]\d{8,}$~D',
                ],
                [
                    'name' => 'Mozambique',
                    'code' => '+258',
                    'regex' => '~^258[1-9]\d{8,}$~D',
                ],
                [
                    'name' => 'Uganda',
                    'code' => '+256',
                    'regex' => '~^256[1-9]\d{8,}$~D',
                ]
            ];


        DB::table('countries')->insert($countries);
    }
}
