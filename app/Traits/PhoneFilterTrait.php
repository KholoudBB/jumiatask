<?php


namespace App\Traits;

use App\Services\CustomerClass;

trait PhoneFilterTrait
{

    /**
     * filter a listing of the phones.
     * @param $query
     * @param $request
     * @return mixed
     */

    protected function filter($query, $request)
    {
        //filter by country

        if ($request->country) {
            $query->where(function ($query) use ($request) {
                return $query->where('countries.id', $request->country);

            });
        }
        $countries = $query->get();

        //append state to the countries collection

        foreach ($countries as $country) {
            $customer = new CustomerClass();
            $customer->setCountry($country);
            $customer->setPhone($country->phone);
            $customer->isValid();
            $country->valid = $customer->isValid() ? 'Valid' : 'Invalid';
        }

        //Filter valid and invalid phone numbers

        if (($request->filter)) {
            if ($request->filter == 1) {
                $countries = $countries->filter(function ($item) {
                    return $item->valid == "Valid";
                });
            } elseif ($request->filter == 2) {
                $countries = $countries->filter(function ($item) {
                    return $item->valid == "Invalid";
                });
            }
        }

        return $countries;
    }
}
