<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $table = 'countries';


    public function phones()
    {
        return $this->hasMany(Phone::class);

    }

}
