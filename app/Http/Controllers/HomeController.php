<?php

namespace App\Http\Controllers;

use App\Traits\PhoneFilterTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    use PhoneFilterTrait;

    public function index(Request $request)
    {
        $countriesQuery = DB::table('countries')
            ->join('customers', 'countries.id', '=', 'customers.country_id')
            ->select('countries.*', 'customers.phone');

        $countries = $this->filter($countriesQuery, $request);

        return response()->json($countries);
    }
}
