<?php

namespace app\Services;

use App\Phone;
use Interfaces\CustomerInterface;

class CustomerClass implements CustomerInterface
{
    /**
     * @var  $country
     */
    private $country;

    /**
     * @var Phone $phone
     */
    private $phone;


    public function setCountry($country): void
    {
        $this->country = $country;
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function isValid(): bool
    {
        return preg_match($this->country->regex, $this->phone);
    }

}
