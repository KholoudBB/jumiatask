<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded=[];

    protected $table = 'customers';

    public function country()
    {
        return $this->belongsTo(Country::class);

    }


}
