<?php

namespace Interfaces;

interface CustomerInterface
{

    /**
     * @param $country
     */
    public function setCountry($country): void;


    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void;


    public function isValid(): bool;

}
